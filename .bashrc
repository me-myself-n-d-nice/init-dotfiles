# .bashrc

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

export PATH=$PATH:~/.bin/:~/.bin/ltx/
source ~/.bash_private_env

alias ls='ls --color=auto'
PS1='[\u@\h \W]\$ '

export DOWNLOADS=~/dl
export I3BLOCKS=~/.config/i3/block-scripts/
export EDITOR=/usr/bin/nvim

# needed for gpg pipe/ioctl fix
export GPG_TTY=$(tty)

# SYNC ALL BASH
#unset HISTSIZE
#unset HISTFILESIZE
export HISTSIZE=900000
export HISTFILESIZE=900000
export HISTIGNORE='ls'
# Avoid duplicates
export HISTCONTROL=ignoredups:erasedups  
# When the shell exits, append to the history file instead of overwriting it
shopt -s histappend

# After each command, append to the history file and reread it
#export PROMPT_COMMAND="${PROMPT_COMMAND:+$PROMPT_COMMAND$'\n'}history -a; history -c; history -r"
# END SYNC

# cat ~/.cache/wal/sequences &

# wait a cycle before applying transparency
# [[ $DISPLAY ]] && transset -a 0.8 > /dev/null
# sleep 0.01
# [[ $DISPLAY ]] && transset -a 0.8 > /dev/null
# sleep 0.01
# [[ $DISPLAY ]] && transset -a 0.8 > /dev/null

#NVM
export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh" --no-use # This loads nvm - D-Nice fast with --no-use
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion

# powerline + bash vi
source ~/.bash-powerline.sh
set -o vi

# NIM enablement

function useNim() {
  echo $PATH | grep -P "/home/$USER/.nimble/bin"
  if [ $? -eq 0 ]; then
    echo "Nim already active..."
    return 1
  fi
  export PATH=/home/user/.nimble/bin:$PATH
  echo "Added Nim to PATH"
  return 0
}
