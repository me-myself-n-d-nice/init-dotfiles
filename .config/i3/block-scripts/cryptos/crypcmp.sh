#!/bin/bash

#res=$(curl -s 'https://api.cryptowat.ch/markets/prices'|jq '.result')
btc=$(curl -s 'https://min-api.cryptocompare.com/data/generateAvg?fsym=BTC&tsym=USD&markets=CCCAGG&e=CCCAGG')
eth=$(curl -s 'https://min-api.cryptocompare.com/data/generateAvg?fsym=ETH&tsym=USD&markets=CCCAGG&e=CCCAGG')
zec=$(curl -s 'https://min-api.cryptocompare.com/data/generateAvg?fsym=ZEC&tsym=USD&markets=CCCAGG&e=CCCAGG')
btcusd=$(echo $btc|jq -r '.DISPLAY.PRICE')
ethusd=$(echo $eth|jq -r '.DISPLAY.PRICE')
zecusd=$(echo $zec|jq -r '.DISPLAY.PRICE')
btcm=$(echo $btc|jq -r '.RAW.CHANGE24HOUR')
btcm=$(echo ${btcm%.*})
ethm=$(echo $eth|jq -r '.RAW.CHANGE24HOUR')
ethm=$(echo ${ethm%.*})
zecm=$(echo $zec|jq -r '.RAW.CHANGE24HOUR')
zecm=$(echo ${zecm%.*})

btcp=$(echo $btc|jq -r '.DISPLAY.CHANGEPCT24HOUR')
ethp=$(echo $eth|jq -r '.DISPLAY.CHANGEPCT24HOUR')
zecp=$(echo $zec|jq -r '.DISPLAY.CHANGEPCT24HOUR')

RED='\033[0;31m'
GRN='\033[1;32m'
NC='\033[0m'

btccol=$(((btcm > 0)) && echo "#22FF22" || echo "#FF0000")
ethcol=$(((ethm > 0)) && echo "#22FF22" || echo "#FF0000")
zeccol=$(((zecm > 0)) && echo "#22FF22" || echo "#FF0000")

#echo $btcusd $ethusd $zecusd

printf ":${btcusd#\$}<span color='$btccol'> ($btcp%%) </span>"
printf "E:${ethusd#\$}<span color='$ethcol'> ($ethp%%) </span>"
printf "Z:${zecusd#\$}<span color='$zeccol'> ($zecp%%) </span>"
